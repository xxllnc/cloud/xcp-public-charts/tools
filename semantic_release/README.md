# semantic-release

Semantic release is used in the gitlab-ci to automatically
determine the next Version number and add this as a Tag
to the commit.

In this directory is the Gitlab-ci configuration to
create the docker image that is used to determine the next version
