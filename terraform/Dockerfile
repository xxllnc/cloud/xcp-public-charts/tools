# https://hub.docker.com/_/python
# Terraform: https://github.com/hashicorp/terraform/releases
# Terragrunt: https://github.com/gruntwork-io/terragrunt/releases
# Terraform docs: https://github.com/terraform-docs/terraform-docs/releases
# Terraform lint: https://github.com/terraform-linters/tflint/releases
# tfsec: https://github.com/tfsec/tfsec/releases
FROM python:3.11-slim
ARG ARCH=amd64

# Terraform >= 1.6 uses a "Business Source License", therefore stick to 1.5.x which has MPL license
ENV TF_VERSION=1.5.7
ENV TG_VERSION=0.52.5
ENV TF_DOCS_VERSION=0.16.0
ENV TFLINT_VERSION=0.48.0
ENV TFSEC_VERSION=1.28.4

RUN apt-get update \
    && apt-get install -y curl unzip build-essential git jq

COPY terraform/requirements.txt .

RUN python3 -m pip install --upgrade pip \
    && pip3 install -r requirements.txt

RUN curl -sLo /tmp/terraform.zip https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_${ARCH}.zip \
    && unzip /tmp/terraform.zip -d /usr/local/bin  \
    && rm /tmp/terraform.zip \
    && chmod +x /usr/local/bin/terraform \
    \
    && curl -sLo /tmp/terraform-docs.tar.gz https://github.com/terraform-docs/terraform-docs/releases/download/v${TF_DOCS_VERSION}/terraform-docs-v${TF_DOCS_VERSION}-linux-${ARCH}.tar.gz \
    && tar xf /tmp/terraform-docs.tar.gz -C /usr/local/bin/  \
    && rm /tmp/terraform-docs.tar.gz \
    && chmod +x /usr/local/bin/terraform-docs \
    \
    && curl -sLo /usr/local/bin/terragrunt https://github.com/gruntwork-io/terragrunt/releases/download/v${TG_VERSION}/terragrunt_linux_${ARCH} \
    && chmod +x /usr/local/bin/terragrunt \
    \
    && curl -sLo /tmp/tflint.zip https://github.com/terraform-linters/tflint/releases/download/v${TFLINT_VERSION}/tflint_linux_${ARCH}.zip \
    && unzip /tmp/tflint.zip -d /usr/local/bin  \
    && rm /tmp/tflint.zip \
    && chmod +x /usr/local/bin/tflint \
    \
    && curl -sLo /usr/local/bin/tfsec https://github.com/tfsec/tfsec/releases/download/v${TFSEC_VERSION}/tfsec-linux-${ARCH} \
    && chmod +x /usr/local/bin/tfsec
